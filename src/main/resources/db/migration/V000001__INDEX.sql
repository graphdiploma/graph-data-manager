CREATE UNIQUE INDEX CONCURRENTLY graph_min_one_extension_source_ext on GRAPH_MIN_ONE_EXTENSION USING btree(source_graph_id, ext_graph_id);

CREATE UNIQUE INDEX CONCURRENTLY graph_isomorphism_source_ext on GRAPH_ISOMORPHISM USING btree(first_graph_id, second_graph_id);