create type graph_encoding_format as enum (
    'GRAPH6',
    'DIGRAPH6'
);

create type graph_ext_type as enum (
    'VERTEX',
    'EDGE'
);

create type graph_computation_type as enum (
    'VERTEX_EXTENSION',
    'EDGE_EXTENSION',
    'ISOMORPHISM'
);

CREATE TABLE GRAPH(
    id uuid primary key,
    adj_matrix_encoded varchar unique,
    encoding_format graph_encoding_format
);

CREATE TABLE GRAPH_MIN_ONE_EXTENSION(
    id uuid primary key,
    source_graph_id uuid references GRAPH(id),
    ext_graph_id uuid references GRAPH(id),
    ext_type graph_ext_type
);

CREATE TABLE GRAPH_ISOMORPHISM(
    id uuid primary key,
    first_graph_id uuid references GRAPH(id),
    second_graph_id uuid references GRAPH(id)
);

CREATE TABLE GRAPH_COMPUTATION(
    id uuid primary key,
    comp_type graph_computation_type,
    first_graph_id uuid references GRAPH(id),
    second_graph_id uuid references GRAPH(id),
    k_current integer,
    k_max integer,
    computed bool
);