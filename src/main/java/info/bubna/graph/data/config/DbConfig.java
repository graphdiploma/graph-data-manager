package info.bubna.graph.data.config;

import info.bubna.graph.data.exception.InitException;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;

import java.io.File;

@Slf4j
@Value
public class DbConfig {
    private final String jdbcUrl;
    private final String login;
    private final String pass;

    public DbConfig() {
        Configurations configs = new Configurations();
        try {
            Configuration config = configs.properties(new File("db.properties"));
            jdbcUrl = config.getString("db.url");
            login = config.getString("db.login");
            pass = config.getString("db.pass");
            // access configuration properties
        } catch (ConfigurationException cex) {
            log.error("init error;", cex);
            throw new InitException(cex);
        }
    }
}
