package info.bubna.graph.data.config;

import info.bubna.graph.data.exception.InitException;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;

import java.io.File;

@Slf4j
@Value
public class AppConfig {
    private final int verticesCountFrom;
    private final int verticesCountTo;

    public AppConfig() {
        Configurations configs = new Configurations();
        try {
            Configuration config = configs.properties(new File("app.properties"));
            verticesCountFrom = config.getInt("app.vertices.count.from");
            verticesCountTo = config.getInt("app.vertices.count.to");
            // access configuration properties
        } catch (ConfigurationException cex) {
            log.error("init error;", cex);
            throw new InitException(cex);
        }
    }
}
