package info.bubna.graph.data.utils;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import info.bubna.graph.data.config.DbConfig;
import info.bubna.graph.data.dao.ComputationDao;
import info.bubna.graph.data.exception.InitException;
import info.bubna.graph.data.generated.tables.daos.GraphComputationDao;
import info.bubna.graph.data.generated.tables.daos.GraphDao;
import info.bubna.graph.data.generated.tables.daos.GraphIsomorphismDao;
import info.bubna.graph.data.generated.tables.daos.GraphMinOneExtensionDao;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

@Slf4j
@Value
public class DBTools {
    private final DbConfig cfg;
    private final HikariDataSource ds;
    private final DSLContext dsl;
    private final GraphDao graphDao;
    private final ComputationDao computationDao;
    private final GraphComputationDao graphComputationDao;
    private final GraphIsomorphismDao graphIsomorphismDao;
    private final GraphMinOneExtensionDao graphMinOneExtensionDao;

    public DBTools(DbConfig cfg) {
        this.cfg = cfg;
        try {
            HikariConfig config = new HikariConfig();
            config.setJdbcUrl(cfg.getJdbcUrl());
            config.setUsername(cfg.getLogin());
            config.setPassword(cfg.getPass());
            config.addDataSourceProperty("cachePrepStmts", "true");
            config.addDataSourceProperty("prepStmtCacheSize", "250");
            config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
            ds = new HikariDataSource(config);
            dsl = DSL.using(ds, SQLDialect.POSTGRES);

            graphDao = new GraphDao(dsl.configuration());
            graphComputationDao = new GraphComputationDao(dsl.configuration());
            graphIsomorphismDao = new GraphIsomorphismDao(dsl.configuration());
            graphMinOneExtensionDao = new GraphMinOneExtensionDao(dsl.configuration());
            computationDao = new ComputationDao(dsl, graphDao);
        } catch (Exception e) {
            log.error("init error;", e);
            throw new InitException(e);
        }
    }
}
