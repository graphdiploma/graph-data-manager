package info.bubna.graph.data.utils;

import info.bubna.graph.data.exception.InitException;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.TextMessage;

@Slf4j
@Value
@EqualsAndHashCode(callSuper = true)
public class ActiveMQConnectionConsumer extends ActiveMQConnection {

    private MessageConsumer consumer;

    public ActiveMQConnectionConsumer(String host, String queue, String login, String pass) {
        super(host, queue, login, pass);
        try {
            consumer = session.createConsumer(destination);
        } catch (JMSException e) {
            log.error("init error;", e);
            throw new InitException(e);
        }
    }

    public TextMessage consumeTextMessage() throws JMSException {
        var msg = consumer.receive(5000);
        if (msg instanceof TextMessage) {
            return (TextMessage) msg;
        } else return null;
    }
}