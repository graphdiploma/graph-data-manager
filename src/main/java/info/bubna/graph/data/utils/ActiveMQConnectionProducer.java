package info.bubna.graph.data.utils;

import info.bubna.graph.data.exception.InitException;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.MessageProducer;

@Slf4j
@Value
@EqualsAndHashCode(callSuper = true)
public class ActiveMQConnectionProducer extends ActiveMQConnection {

    private MessageProducer producer;

    public ActiveMQConnectionProducer(String host, String queue, String login, String pass) {
        super(host, queue, login, pass);
        try {
            ((org.apache.activemq.ActiveMQConnection) connection).setUseAsyncSend(false);
            producer = session.createProducer(destination);
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
        } catch (JMSException e) {
            log.error("init error;", e);
            throw new InitException(e);
        }
    }

    public void createAndSendMessage(String text) throws JMSException {
        var msg = session.createTextMessage(text);
        msg.setJMSDeliveryMode(DeliveryMode.NON_PERSISTENT);
        producer.send(msg);
    }
}


