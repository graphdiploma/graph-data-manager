package info.bubna.graph.data.exception;

public class InitException extends RuntimeException {
    public InitException(String msg) {
        super(msg);
    }

    public InitException(Exception e) {
        super(e);
    }
}
