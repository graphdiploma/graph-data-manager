package info.bubna.graph.data.exception;

public class RichKMaxException extends Exception {
    public RichKMaxException(String msg) {
        super(msg);
    }

    public RichKMaxException(Exception e) {
        super(e);
    }
}
