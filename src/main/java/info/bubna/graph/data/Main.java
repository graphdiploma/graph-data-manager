package info.bubna.graph.data;

import com.google.gson.Gson;
import info.bubna.graph.data.config.ActiveMQConfig;
import info.bubna.graph.data.config.AppConfig;
import info.bubna.graph.data.config.DbConfig;
import info.bubna.graph.data.exception.RichKMaxException;
import info.bubna.graph.data.model.ComputationDTO;
import info.bubna.graph.data.model.ComputationType;
import info.bubna.graph.data.service.IActiveMQService;
import info.bubna.graph.data.service.IDigraph6ConversionService;
import info.bubna.graph.data.service.IDirectgOrientedCycleCreationService;
import info.bubna.graph.data.service.IGraph6CreationService;
import info.bubna.graph.data.service.IMin1ExtPermutationProducerService;
import info.bubna.graph.data.service.impl.ActiveMQService;
import info.bubna.graph.data.service.impl.ComputationManagerService;
import info.bubna.graph.data.service.impl.Digraph6ConversionService;
import info.bubna.graph.data.service.impl.DirectgOrientedCycleCreationService;
import info.bubna.graph.data.service.impl.Graph6CreationService;
import info.bubna.graph.data.service.impl.MainService;
import info.bubna.graph.data.service.impl.Min1ExtPermutationProducerService;
import info.bubna.graph.data.utils.DBTools;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Slf4j
public class Main {

    public static final Gson gson = new Gson();

    public static final AppConfig appConfig = new AppConfig();
    public static final ActiveMQConfig mqConfig = new ActiveMQConfig();
    public static final DbConfig dbConfig = new DbConfig();
    public static final DBTools dbTools = new DBTools(dbConfig);

    public static final IActiveMQService mqService = new ActiveMQService(gson, mqConfig);
    public static final IGraph6CreationService graph6CreationService = new Graph6CreationService();
    public static final IDirectgOrientedCycleCreationService directgService = new DirectgOrientedCycleCreationService();
    public static final IDigraph6ConversionService digraph6ConversionService = new Digraph6ConversionService();
    public static final IMin1ExtPermutationProducerService min1ExtPermutationProducerService =
            new Min1ExtPermutationProducerService(digraph6ConversionService);
    public static final ComputationManagerService computationManagerService = new ComputationManagerService(
            dbTools.getDsl(),
            dbTools.getGraphDao(),
            dbTools.getComputationDao(),
            dbTools.getGraphComputationDao(),
            dbTools.getGraphIsomorphismDao(),
            dbTools.getGraphMinOneExtensionDao()
    );
    public static final MainService mainService = new MainService(mqService, computationManagerService, min1ExtPermutationProducerService);

    /**
     * Unit tests the {@code GraphGenerator} library.
     *
     * @param args the command-line arguments
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        var currentVerticesCount = appConfig.getVerticesCountFrom();
        while (currentVerticesCount <= appConfig.getVerticesCountTo()) {
            var res = graph6CreationService.create(currentVerticesCount);
            for (var g : directgService.generate(res)) {
                var gAdjMatrix = digraph6ConversionService.toAdjMatrix(g);
                try {
                    var listeningThread = mainService.processResults(g, gAdjMatrix, ComputationType.VERTICES_EMBEDDED);
                    min1ExtPermutationProducerService.producePermutations(gAdjMatrix, true, it ->
                            CompletableFuture.runAsync(() ->
                                    mqService.write(ComputationDTO.builder()
                                            .id(UUID.randomUUID())
                                            .digraph6Source(g)
                                            .digraph6Target(it.getRight())
                                            .kCurrent(it.getLeft().getKCurrent())
                                            .kMax(it.getLeft().getKMax())
                                            .computationType(ComputationType.VERTICES_EMBEDDED)
                                            .build()
                                    )
                            )
                    );
                    mqService.write(ComputationDTO.builder()
                            .id(UUID.randomUUID())
                            .computationType(ComputationType.EOC)
                            .build()
                    );
                    listeningThread.join();

                } catch (RichKMaxException kMaxE) {
                    log.error("k max riched, but vertices extensions not found", kMaxE);
                }
                try {
                    var listeningThread = mainService.processResults(g, gAdjMatrix, ComputationType.EDGES_EMBEDDED);
                    min1ExtPermutationProducerService.producePermutations(gAdjMatrix, false, it ->
                            CompletableFuture.runAsync(() ->
                                    mqService.write(ComputationDTO.builder()
                                            .id(UUID.randomUUID())
                                            .digraph6Source(g)
                                            .digraph6Target(it.getRight())
                                            .kCurrent(it.getLeft().getKCurrent())
                                            .kMax(it.getLeft().getKMax())
                                            .computationType(ComputationType.EDGES_EMBEDDED)
                                            .build()
                                    )
                            )
                    );
                    mqService.write(ComputationDTO.builder()
                            .id(UUID.randomUUID())
                            .computationType(ComputationType.EOC)
                            .build()
                    );
                    listeningThread.join();
                } catch (RichKMaxException kMaxE) {
                    log.error("k max riched, but edges extensions not found", kMaxE);
                }
                var listeningThread = mainService.processIsomorphisms();
                computationManagerService.produceIsomorphismCheck(g, it ->
                        CompletableFuture.runAsync(() ->
                                mqService.write(ComputationDTO.builder()
                                        .id(UUID.randomUUID())
                                        .digraph6Source(g)
                                        .digraph6Target(it)
                                        .kCurrent(-1)
                                        .kMax(-1)
                                        .computationType(ComputationType.ISOMORPHISM)
                                        .build()
                                )
                        )
                );
                mqService.write(ComputationDTO.builder()
                        .id(UUID.randomUUID())
                        .computationType(ComputationType.EOC)
                        .build()
                );
                listeningThread.join();
            }
            currentVerticesCount++;
        }

        System.out.println();
    }
}
