package info.bubna.graph.data.service.impl;

import info.bubna.graph.data.service.IGraph6CreationService;

import java.util.LinkedList;

public class Graph6CreationService implements IGraph6CreationService {

    public Graph6CreationService() {}

    public String create(int verticesCount) {
        LinkedList<Integer> adjMatrix = new LinkedList<>();
        for (int i = 0; i < verticesCount - 2; i++) {
            for (int j = 0; j < i; j++) adjMatrix.add(0);
            adjMatrix.add(1);
        }
        adjMatrix.add(1);
        for (int j = 0; j < verticesCount - 3; j++) {
            adjMatrix.add(0);
        }
        adjMatrix.add(1);

        var additionalZeros = adjMatrix.size() % 6;
        for (int i = 0; i < additionalZeros; i++) {
            adjMatrix.add(0);
        }

        LinkedList<Integer> tmp = new LinkedList<>();
        tmp.add(63 + verticesCount);
        int a = 5;
        int graph6Number = 0;
        for (Integer adjMatrix1 : adjMatrix) {
            if (adjMatrix1 != 0) graph6Number += Math.pow(2, a);
            a--;
            if (a < 0) {
                a = 5;
                tmp.add(63 + graph6Number);
                graph6Number = 0;
            }
        }
        var res = new StringBuilder();
        tmp.forEach(it -> res.append((char) ((int) it)));
        return res.toString();
    }
}
