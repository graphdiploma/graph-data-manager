package info.bubna.graph.data.service.impl;

import info.bubna.graph.data.dao.ComputationDao;
import info.bubna.graph.data.generated.enums.GraphComputationType;
import info.bubna.graph.data.generated.enums.GraphEncodingFormat;
import info.bubna.graph.data.generated.enums.GraphExtType;
import info.bubna.graph.data.generated.tables.daos.GraphComputationDao;
import info.bubna.graph.data.generated.tables.daos.GraphDao;
import info.bubna.graph.data.generated.tables.daos.GraphIsomorphismDao;
import info.bubna.graph.data.generated.tables.daos.GraphMinOneExtensionDao;
import info.bubna.graph.data.generated.tables.pojos.Graph;
import info.bubna.graph.data.generated.tables.pojos.GraphComputation;
import info.bubna.graph.data.generated.tables.pojos.GraphIsomorphism;
import info.bubna.graph.data.generated.tables.pojos.GraphMinOneExtension;
import info.bubna.graph.data.model.ComputationResultDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jooq.DSLContext;

import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

@Slf4j
@RequiredArgsConstructor
public class ComputationManagerService {
    private final DSLContext dsl;
    private final GraphDao graphDao;
    private final ComputationDao computationDao;
    private final GraphComputationDao graphComputationDao;
    private final GraphIsomorphismDao graphIsomorphismDao;
    private final GraphMinOneExtensionDao graphMinOneExtensionDao;

    public boolean checkComputationsCompleted(String source, GraphComputationType computationType, int kCurrent) {
        return computationDao.openComputationsCount(source, computationType, kCurrent) == 0;
    }

    public void produceIsomorphismCheck(String source, Consumer<String> resultProcessor) {
        computationDao.fetchGraphExtensions(source, true).forEach(resultProcessor);
    }

    public boolean checkMinExtensionFound(String source, GraphExtType extType) {
        return computationDao.extensionsCount(source, extType) > 0;
    }

    public void processComputationResult(ComputationResultDTO result) {
        if (result.getResult()) {
            switch (result.getComputationType()) {
                case VERTICES_EMBEDDED:
                    processExtensionsResults(result, GraphExtType.VERTEX);
                    break;
                case EDGES_EMBEDDED:
                    processExtensionsResults(result, GraphExtType.EDGE);
                    break;
                case ISOMORPHISM:
                    processIsomorphism(result);
            }
        }
    }

    private void processExtensionsResults(ComputationResultDTO computation, GraphExtType type) {
        Graph sourceGraph = graphDao.fetchOneByAdjMatrixEncoded(computation.getDigraph6Source());
        if (sourceGraph == null) {
            sourceGraph = new Graph(UUID.randomUUID(), computation.getDigraph6Source(), GraphEncodingFormat.DIGRAPH6);
            graphDao.insert(sourceGraph);
        }
        Graph target = new Graph(UUID.randomUUID(), computation.getDigraph6Target(), GraphEncodingFormat.DIGRAPH6);
        graphDao.insert(target);
        GraphMinOneExtension extension = new GraphMinOneExtension(
                UUID.randomUUID(),
                sourceGraph.getId(),
                target.getId(),
                type
        );
        graphMinOneExtensionDao.insert(extension);
    }

    private void processIsomorphism(ComputationResultDTO computation) {
        Graph source = graphDao.fetchOneByAdjMatrixEncoded(computation.getDigraph6Source());
        Graph target = graphDao.fetchOneByAdjMatrixEncoded(computation.getDigraph6Target());
        GraphIsomorphism isomorphism = new GraphIsomorphism(
                UUID.randomUUID(), source.getId(), target.getId()
        );
        graphIsomorphismDao.insert(isomorphism);
    }

    public UUID registerComputation(
            int kCurrent, int kMax, String source, String target, GraphComputationType graphComputationType
    ) {
        Graph sourceGraph = graphDao.fetchOneByAdjMatrixEncoded(source);
        if (sourceGraph == null) {
            sourceGraph = new Graph(UUID.randomUUID(), source, GraphEncodingFormat.DIGRAPH6);
            graphDao.insert(sourceGraph);
        }
        var targetRecord = new Graph(UUID.randomUUID(), target, GraphEncodingFormat.DIGRAPH6);
        var computationRecord = new GraphComputation(
                UUID.randomUUID(),
                graphComputationType,
                sourceGraph.getId(),
                targetRecord.getId(),
                kCurrent,
                kMax,
                false
        );
        graphDao.insert(targetRecord);
        graphComputationDao.insert(computationRecord);

        return computationRecord.getId();
    }
}
