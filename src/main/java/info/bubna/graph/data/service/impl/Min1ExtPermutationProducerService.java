package info.bubna.graph.data.service.impl;

import info.bubna.graph.data.exception.RichKMaxException;
import info.bubna.graph.data.model.PermutationsContext;
import info.bubna.graph.data.service.IDigraph6ConversionService;
import info.bubna.graph.data.service.IMin1ExtPermutationProducerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Consumer;

@Slf4j
@RequiredArgsConstructor
public class Min1ExtPermutationProducerService implements IMin1ExtPermutationProducerService {

    private final IDigraph6ConversionService digraph6ConversionService;

    private Integer[][] addVertex(Integer[][] adjInMatrix) {
        var verticesCount = adjInMatrix.length;
        var resultAdjMatrix = new Integer[verticesCount + 1][];
        for (int i = 0; i < verticesCount; i++) {
            var newArr = new Integer[verticesCount + 1];
            System.arraycopy(adjInMatrix[i], 0, newArr, 0, verticesCount);
            newArr[verticesCount] = 0;
            resultAdjMatrix[i] = newArr;
        }
        var newArr = new Integer[verticesCount + 1];
        Arrays.fill(newArr, 0);
        resultAdjMatrix[verticesCount] = newArr;
        return resultAdjMatrix;
    }

    private void checkSquareMatrix(Integer[][] matrix) {
        if (matrix.length < 1) throw new IllegalArgumentException("adjInMatrix.length < 1");
        //todo check all lines
        if (matrix.length != matrix[0].length) throw new IllegalArgumentException("adjInMatrix is not a adj matrix");
    }

    public void producePermutations(
            Integer[][] adjInMatrix, int currentK, boolean isVerticesExt, Consumer<Pair<PermutationsContext, String>> onPermutation
    ) throws RichKMaxException {
        log.trace("min1Extension isVerticesExt = {}", isVerticesExt);

        log.trace("min1Extension start");
        checkSquareMatrix(adjInMatrix);

        Integer[][] newAdjMatrix = isVerticesExt ? addVertex(adjInMatrix) : adjInMatrix;

        var verticesCount = adjInMatrix.length;
        var emptyPositions = findEmptyPositions(newAdjMatrix);

        int kMax = isVerticesExt ? (verticesCount * verticesCount) : (verticesCount * (verticesCount - 2));
        if (currentK > kMax) {
            throw new RichKMaxException("computations rich kMax; stopped");
        }
        log.trace("min1Extension kMax - {}", kMax);

        var kMin = currentK == -1 ? isVerticesExt ? (verticesCount / 2) + (verticesCount % 2) : verticesCount / 2 : currentK;
        permutation1(emptyPositions, kMin, 0, new Integer[kMin], newAdjMatrix, onPermutation,
                PermutationsContext.builder()
                        .kCurrent(kMin)
                        .kMax(kMax)
                        .build()
        );
    }

    public void producePermutations(
            Integer[][] adjInMatrix, boolean isVerticesExt, Consumer<Pair<PermutationsContext, String>> onPermutation
    ) throws RichKMaxException {
        producePermutations(adjInMatrix, -1, isVerticesExt, onPermutation);
    }

    private Integer[] findEmptyPositions(Integer[][] adjInMatrix) {
        ArrayList<Integer> resultList = new ArrayList<>();
        var verticesCount = adjInMatrix.length;
        for (int i = 0; i < verticesCount; i++) {
            for (int j = 0; j < verticesCount; j++) {
                var a = adjInMatrix[i][j];
                if (a != 0 || i == j) continue;

                resultList.add(i * verticesCount + j);
            }
        }
        Integer[] result = new Integer[resultList.size()];
        resultList.toArray(result);
        return result;
    }

    private void addEdges(Integer[][] adjInMatrix, Integer[] edgesPositions) {
        var verticesCount = adjInMatrix.length;
        for (Integer a : edgesPositions) {
            int x = a / verticesCount;
            int y = a % verticesCount;
            adjInMatrix[x][y] = 1;
        }
    }

    private void permutation1(
            Integer[] source, int k, int sourceStartPos, Integer[] targetPermutation,
            Integer[][] targetEmptyMatrix,
            Consumer<Pair<PermutationsContext, String>> onPermutation,
            PermutationsContext ctx
    ) {
        if (k == 0) {
            var targetMatrix = targetEmptyMatrix.clone();
            for (int i = 0; i < targetEmptyMatrix.length; i++) targetMatrix[i] = targetEmptyMatrix[i].clone();
            addEdges(targetMatrix, targetPermutation);
            onPermutation.accept(Pair.of(ctx, digraph6ConversionService.toDigraph6(targetMatrix)));
            return;
        }
        for (int i = sourceStartPos; i <= source.length - k; i++) {
            targetPermutation[targetPermutation.length - k] = source[i];
            permutation1(source, k - 1, i + 1, targetPermutation, targetEmptyMatrix, onPermutation, ctx);
        }
    }
}