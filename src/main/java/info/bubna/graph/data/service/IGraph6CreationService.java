package info.bubna.graph.data.service;

public interface IGraph6CreationService {

    /**
     * Создает неориентированный цикл из verticesCount вершин и сохраняет в строку формата graph6
     * @param verticesCount кол-во вершин
     * @return graph6 формата граф
     */
    String create(int verticesCount);
}
