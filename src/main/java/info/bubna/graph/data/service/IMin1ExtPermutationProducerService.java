package info.bubna.graph.data.service;

import info.bubna.graph.data.exception.RichKMaxException;
import info.bubna.graph.data.model.PermutationsContext;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;
import java.util.function.Consumer;

public interface IMin1ExtPermutationProducerService {

    /**
     * Ищет пустые возможные места для установки ребери отправляет в mq
     * (учитывает тип поиска - вершинное или реберное расширение)
     *  @param adjInMatrix   матрица смежности
     * @param isVerticesExt
     */
    void producePermutations(Integer[][] adjInMatrix, boolean isVerticesExt, Consumer<Pair<PermutationsContext, String>> onPermutation) throws RichKMaxException;

    /**
     * Ищет пустые возможные места для установки ребери отправляет в mq
     * (учитывает тип поиска - вершинное или реберное расширение)
     * (учитывает текущее k)
     *  @param adjInMatrix   матрица смежности
     * @param currentK  текущее k
     * @param isVerticesExt
     */
    void producePermutations(Integer[][] adjInMatrix, int currentK, boolean isVerticesExt, Consumer<Pair<PermutationsContext, String>> onPermutation) throws RichKMaxException;
}