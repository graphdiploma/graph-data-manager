package info.bubna.graph.data.service.impl;

import info.bubna.graph.data.exception.RichKMaxException;
import info.bubna.graph.data.generated.enums.GraphComputationType;
import info.bubna.graph.data.generated.enums.GraphExtType;
import info.bubna.graph.data.model.ComputationDTO;
import info.bubna.graph.data.model.ComputationType;
import info.bubna.graph.data.service.IActiveMQService;
import info.bubna.graph.data.service.IMin1ExtPermutationProducerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Slf4j
@RequiredArgsConstructor
public class MainService {

    private final IActiveMQService mqService;
    private final ComputationManagerService computationManagerService;
    private final IMin1ExtPermutationProducerService min1ExtPermutationProducerService;

    private Thread extensionThread(String g, Integer[][] gAdjMatrix, ComputationType compType, GraphExtType extType) {
        return new Thread(() -> {
            var lastKCurrent = -1;
            var verticesExtensionsComputed = false;
            var eoc = false;
            while (!verticesExtensionsComputed) {
                try {
                    var compResult = mqService.readAll();

                    if (compResult != null && !eoc) {
                        for (var r : compResult) {
                            if (r.getComputationType() == ComputationType.EOC) eoc = true;
                            else {
                                computationManagerService.processComputationResult(r);
                                lastKCurrent = r.getKCurrent();
                            }
                        }
                        continue;
                    } else if (eoc && computationManagerService.checkMinExtensionFound(g, extType)) {
                        verticesExtensionsComputed = true;
                        continue;
                    }
                    try {
                        min1ExtPermutationProducerService.producePermutations(
                                gAdjMatrix, lastKCurrent + 1, extType == GraphExtType.VERTEX, it ->
                                        CompletableFuture.runAsync(() -> mqService.write(ComputationDTO.builder()
                                                .id(UUID.randomUUID())
                                                .digraph6Source(g)
                                                .digraph6Target(it.getRight())
                                                .kCurrent(it.getLeft().getKCurrent())
                                                .kMax(it.getLeft().getKMax())
                                                .computationType(compType)
                                                .build()
                                        ))
                        );
                    } catch (RichKMaxException e) {
                        log.error("riched k max, but extensions not found", e);
                        break;
                    }
                } catch (Exception e) {
                    log.error("ex in listening thread; ", e);
                }
            }
        });
    }

    private Thread isomorphismThread() {
        return new Thread(() -> {
            while (true) {
                try {
                    var compResult = mqService.readAll();
                    var eoc = false;
                    if (compResult != null) {
                        for (var r : compResult) {
                            if (r.getComputationType() == ComputationType.EOC) eoc = true;
                            else computationManagerService.processComputationResult(r);
                        }
                        if (eoc) break;
                    } else break;
                } catch (Exception e) {
                    log.error("ex in listening thread; ", e);
                }
            }
        });
    }

    public Thread processIsomorphisms() {
        var t = isomorphismThread();
        t.start();
        return t;
    }

    //TODO process isomorphism
    public Thread processResults(String g, Integer[][] gAdjMatrix, ComputationType computationType) {
        var graphExtType = computationType == ComputationType.VERTICES_EMBEDDED ? GraphExtType.VERTEX
                : computationType == ComputationType.EDGES_EMBEDDED ? GraphExtType.EDGE
                : null;

        var t = extensionThread(g, gAdjMatrix, computationType, graphExtType);
        t.start();
        return t;
    }
}