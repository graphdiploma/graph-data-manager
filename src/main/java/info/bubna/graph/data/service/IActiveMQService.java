package info.bubna.graph.data.service;

import info.bubna.graph.data.model.ComputationDTO;
import info.bubna.graph.data.model.ComputationResultDTO;
import org.apache.activemq.Closeable;

import java.util.List;

public interface IActiveMQService extends Closeable {
    /**
     * Отправить вычисления
     * @param computation единица вычисления
     */
    void write(ComputationDTO computation);

    /**
     * Получить результат вычислений
     * @return единица результата вычислений
     */
    List<ComputationResultDTO> readAll();
}
