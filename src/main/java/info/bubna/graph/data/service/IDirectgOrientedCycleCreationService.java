package info.bubna.graph.data.service;

import java.io.IOException;
import java.util.List;

public interface IDirectgOrientedCycleCreationService {

    /**
     * Генерирует все ориентации цикла
     * @param notOrientedCycle исходный неориентированный цикл
     * @return варианты ориентаций исходного цикла
     */
    List<String> generate(String notOrientedCycle) throws IOException, InterruptedException;
}
