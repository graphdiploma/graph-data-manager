package info.bubna.graph.data.service.impl;

import info.bubna.graph.data.service.IDirectgOrientedCycleCreationService;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class DirectgOrientedCycleCreationService implements IDirectgOrientedCycleCreationService {
    @Override
    public List<String> generate(String notOrientedCycle) throws IOException, InterruptedException {
        var processB = new ProcessBuilder("/bin/sh", "-c", "echo '" + notOrientedCycle + "' | /usr/local/nauty26r12/directg -oq");
        processB.directory(new File("/"));
        processB.redirectErrorStream(true);
        var process = processB.start();
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(process.getInputStream()));

        var rList = new ArrayList<String>();
        String line;
        while ((line = reader.readLine()) != null) {
            rList.add(line);
        }

        int exitVal = process.waitFor();
        if (exitVal != 0) throw new RuntimeException("process finished with err code " + exitVal);

        return rList;
    }
}
