package info.bubna.graph.data.service.impl;

import com.google.gson.Gson;
import info.bubna.graph.data.config.ActiveMQConfig;
import info.bubna.graph.data.model.ComputationDTO;
import info.bubna.graph.data.model.ComputationResultDTO;
import info.bubna.graph.data.service.IActiveMQService;
import info.bubna.graph.data.utils.ActiveMQConnectionConsumer;
import info.bubna.graph.data.utils.ActiveMQConnectionProducer;
import lombok.extern.slf4j.Slf4j;

import javax.jms.JMSException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class ActiveMQService implements IActiveMQService {

    private final Gson gson;
    private final ActiveMQConfig cfg;

    private volatile int currentProducerConnection = 0;
    private volatile int currentConsumerConnection = 0;

    private List<ActiveMQConnectionProducer> producerConnectionPool;
    private List<ActiveMQConnectionConsumer> consumerConnectionPool;

    public ActiveMQService(Gson gson, ActiveMQConfig cfg) {
        this.gson = gson;
        this.cfg = cfg;
        // Create a ConnectionFactory
        producerConnectionPool = new ArrayList<>();
        for (int i = 0; i < cfg.getProducerConnectionPoolSize(); i++) {
            producerConnectionPool.add(
                    new ActiveMQConnectionProducer(
                            cfg.getBrokerAddress(), cfg.getCompQueueName(), cfg.getLogin(), cfg.getPass()
                    )
            );
        }
        consumerConnectionPool = new ArrayList<>();
        for (int i = 0; i < cfg.getConsumerConnectionPoolSize(); i++) {
            consumerConnectionPool.add(
                    new ActiveMQConnectionConsumer(
                            cfg.getBrokerAddress(), cfg.getResultQueueName(), cfg.getLogin(), cfg.getPass()
                    )
            );
        }
    }

    private ActiveMQConnectionProducer getProducerRoundRobin() {
        if (currentProducerConnection >= cfg.getProducerConnectionPoolSize() - 1) currentProducerConnection = 0;
        else currentProducerConnection++;

        return producerConnectionPool.get(currentProducerConnection);
    }

    private ActiveMQConnectionConsumer getConsumerRoundRobin() {
        if (currentConsumerConnection >= cfg.getConsumerConnectionPoolSize() - 1) currentConsumerConnection = 0;
        else currentConsumerConnection++;

        return consumerConnectionPool.get(currentConsumerConnection);
    }

    @Override
    public void write(ComputationDTO computation) {
        var currSendRetries = 0;
        var sendingSuccess = false;
        while (currSendRetries < cfg.getSendRetryCount() && !sendingSuccess) {
            var producer = getProducerRoundRobin();
            try {
                // Create a messages
                String text = gson.toJson(computation);
                producer.createAndSendMessage(text);
                sendingSuccess = true;
            } catch (Exception e) {
                log.error("send error", e);
                try {
                    producer.reconnect();
                } catch (Exception e1) {
                    log.error("reconnect failed", e1);
                }
                currSendRetries++;
            }
        }
        if (!sendingSuccess) throw new RuntimeException("cant't send message");
    }

    @Override
    public List<ComputationResultDTO> readAll() {
        var result = new ArrayList<ComputationResultDTO>();
        for (int i = 0; i < cfg.getConsumerConnectionPoolSize(); i++) {
            try {
                // Create a messages
                var msg = consumerConnectionPool.get(i).consumeTextMessage();
                if (msg != null) {
                    result.add(gson.fromJson(msg.getText(), ComputationResultDTO.class));
                }
            } catch (Exception e) {
                log.error("receive error", e);
            }
        }
        return result.size() > 0 ? result : null;
    }

    @Override
    public void close() throws JMSException {
        for (var producer : producerConnectionPool) {
            producer.close();
        }
        for (var consumer : consumerConnectionPool) {
            consumer.close();
        }
    }
}
