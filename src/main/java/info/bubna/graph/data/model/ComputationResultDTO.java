package info.bubna.graph.data.model;

import lombok.Builder;
import lombok.Value;

import java.util.UUID;

@Value
@Builder
public class ComputationResultDTO {
    private UUID id;
    private ComputationType computationType;
    private String digraph6Source;
    private String digraph6Target;
    private Integer kCurrent;
    private Integer kMax;
    private Boolean result;
}
