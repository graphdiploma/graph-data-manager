package info.bubna.graph.data.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class PermutationsContext {
    private final int kCurrent;
    private final int kMax;
}
