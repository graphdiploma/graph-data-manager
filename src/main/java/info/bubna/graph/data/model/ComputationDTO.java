package info.bubna.graph.data.model;

import lombok.Builder;
import lombok.Value;

import java.util.UUID;

@Value
@Builder
public class ComputationDTO {
    protected UUID id;
    protected ComputationType computationType;
    protected String digraph6Source;
    protected String digraph6Target;
    protected Integer kCurrent;
    protected Integer kMax;
}
