package info.bubna.graph.data.dao;

import info.bubna.graph.data.generated.enums.GraphComputationType;
import info.bubna.graph.data.generated.enums.GraphEncodingFormat;
import info.bubna.graph.data.generated.enums.GraphExtType;
import info.bubna.graph.data.generated.tables.daos.GraphDao;
import info.bubna.graph.data.generated.tables.pojos.GraphComputation;
import info.bubna.graph.data.generated.tables.records.GraphComputationRecord;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.Record2;

import java.util.List;
import java.util.UUID;

import static info.bubna.graph.data.generated.tables.Graph.GRAPH;
import static info.bubna.graph.data.generated.tables.GraphComputation.GRAPH_COMPUTATION;
import static info.bubna.graph.data.generated.tables.GraphIsomorphism.GRAPH_ISOMORPHISM;
import static info.bubna.graph.data.generated.tables.GraphMinOneExtension.GRAPH_MIN_ONE_EXTENSION;
import static org.jooq.impl.DSL.count;

@RequiredArgsConstructor
public class ComputationDao {
    private final DSLContext dsl;
    private final GraphDao graphDao;

    public List<String> fetchGraphExtensions(String source, boolean ignoreIsomorphisms) {
        var sourceId = graphDao.fetchOneByAdjMatrixEncoded(source).getId();
        var head = dsl.select(GRAPH.ADJ_MATRIX_ENCODED, GRAPH_ISOMORPHISM.ID).from(GRAPH_MIN_ONE_EXTENSION)
                .join(GRAPH).on(GRAPH_MIN_ONE_EXTENSION.EXT_GRAPH_ID.eq(GRAPH.ID));
        var isomorphismJoin = head.leftJoin(GRAPH_ISOMORPHISM).on(GRAPH_ISOMORPHISM.FIRST_GRAPH_ID.eq(GRAPH.ID));
        var isomorphismWhere = ignoreIsomorphisms
                ? GRAPH_MIN_ONE_EXTENSION.SOURCE_GRAPH_ID.eq(sourceId)
                : GRAPH_MIN_ONE_EXTENSION.SOURCE_GRAPH_ID.eq(sourceId).and(GRAPH_ISOMORPHISM.ID.isNull());
        return isomorphismJoin.where(isomorphismWhere).fetch().map(Record2::value1);
    }

    public int extensionsCount(String source, GraphExtType extType) {
        return dsl.select(count(GRAPH_MIN_ONE_EXTENSION.ID)).from(GRAPH_MIN_ONE_EXTENSION)
                .join(GRAPH)
                .on(
                        GRAPH.ADJ_MATRIX_ENCODED.eq(source)
                                .and(GRAPH.ENCODING_FORMAT.eq(GraphEncodingFormat.DIGRAPH6))
                                .and(GRAPH.ID.eq(GRAPH_MIN_ONE_EXTENSION.SOURCE_GRAPH_ID))
                )
                .where(GRAPH_MIN_ONE_EXTENSION.EXT_TYPE.eq(extType))
                .fetchOne().value1();
    }

    public int openComputationsCount(String source, GraphComputationType computationType, int kCurrent) {
        return dsl.select(count(GRAPH_COMPUTATION.ID)).from(GRAPH_COMPUTATION)
                .join(GRAPH)
                .on(GRAPH.ADJ_MATRIX_ENCODED.eq(source)
                        .and(GRAPH.ENCODING_FORMAT.eq(GraphEncodingFormat.DIGRAPH6))
                        .and(GRAPH_COMPUTATION.FIRST_GRAPH_ID.eq(GRAPH.ID))
                )
                .where(GRAPH_COMPUTATION.COMP_TYPE.eq(computationType)
                        .and(GRAPH_COMPUTATION.K_CURRENT.eq(kCurrent))
                        .and(GRAPH_COMPUTATION.COMPUTED.eq(false))
                )
                .fetchOne().value1();
    }
}
